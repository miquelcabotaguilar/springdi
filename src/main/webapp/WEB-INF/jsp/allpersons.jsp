<%--
  Created by IntelliJ IDEA.
  User: miquel
  Date: 10/01/2023
  Time: 19:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tothom</title>
</head>
<body>
    <h3>All Persons</h3>

    <ul>
        <c:forEach var="p" items="${persons}">
            <li>${p.name}</li>
        </c:forEach>
    </ul>
</body>
</html>
