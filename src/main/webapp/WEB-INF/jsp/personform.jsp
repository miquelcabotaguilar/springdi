<%--
  Created by IntelliJ IDEA.
  User: miquel
  Date: 10/01/2023
  Time: 18:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Fromulari per person</title>
</head>
<body>
    <h3>New Person</h3>

    <form action="/newPerson" method="post">
        Name: <input type="text" name="name">
        <br>
        <input type="submit" value="send">
    </form>
</body>
</html>
