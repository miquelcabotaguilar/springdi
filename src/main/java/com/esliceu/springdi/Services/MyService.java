package com.esliceu.springdi.Services;

import com.esliceu.springdi.daos.PersonDAO;
import com.esliceu.springdi.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MyService {

    @Autowired
    PersonDAO persondao;

    public int addNumbers(int s, int d) {
        return s+d;
    }

    public void newPerson(String name) {
        Person p = new Person();
        p.setName(name);
        persondao.save(p);
        System.out.println("Dins myservice.newPerson");
        System.out.println(name);
    }

    public List<Person> allPersons() {

        return persondao.all();
    }
}
