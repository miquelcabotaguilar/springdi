package com.esliceu.springdi.Controller;

import com.esliceu.springdi.Services.MyService;
import com.esliceu.springdi.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AppController {
    @Autowired
    MyService service;

    @RequestMapping("/test/{num}")
    public String test(@PathVariable String num, Model m){
        m.addAttribute("message", num);
        return "test";
    }

    @PostMapping("/test/{num}")
    public String testPost(@PathVariable String num, Model m){
        m.addAttribute("message", num);
        return "testPost";
    }

    @RequestMapping("/suma/{num}/{num1}")
    public String suma(@PathVariable int num,@PathVariable int num1, Model m){
        int n3 = service.addNumbers(num, num1);
        m.addAttribute("message", n3);
        return "suma";
    }

    @RequestMapping("/hello")
    public String hello(HttpSession session, Model m){
        Object o = session.getAttribute("counter");
        Integer i = 1;
        if(o == null){
            session.setAttribute("counter", i);
        }else{
            i = (Integer) o;
            i++;
            session.setAttribute("counter", i);
        }
        m.addAttribute("counter", i);
        return "hello";
    }

    @GetMapping("/private/inside")
    public String inside(){
        return "inside";
    }


    @GetMapping("/newPerson")
    public String newPerson(){
        return "personform";
    }

    @PostMapping("/newPerson")
    @ResponseBody
    public String newPersonPost(String name){
        service.newPerson(name);
        return "ok";
    }

    @GetMapping("/allPersons")
    public String allPersons(Model model){
        List<Person> persons= service.allPersons();
        model.addAttribute("persons", persons);
        return "allpersons";
    }

}
